.. IOTA2 documentation master file, created by
   sphinx-quickstart on Wed Jun  6 12:37:42 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to iota2's documentation !
==================================

.. Note::
    This short documentation was written to quickly help users to run iota2, and
    developers to contribute to the project. It is not complete and most parts still in development.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Table of contents
=================

* :doc:`Get iota2 <HowToGetIOTA2>`
* :doc:`iota2 Examples <IOTA2_Example>`
* :doc:`Development recommendations <develop_reco>`
* :doc:`iota2 code architecture : main class <main_class>`
* :doc:`Add steps to iota2 <add_IOTA2_new_step>`
* :doc:`Sentinel-2 Level 3A <sentinel_2_N3A>`

The source code for iota2 is hosted at https://framagit.org/iota2-project/iota2/


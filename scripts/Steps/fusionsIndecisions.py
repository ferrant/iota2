#!/usr/bin/python
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

import IOTA2Step
from Common import ServiceConfigFile as SCF


class fusionsIndecisions(IOTA2Step.Step):
    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        resources_block_name = "noData"
        super(fusionsIndecisions, self).__init__(cfg, cfg_resources_file, resources_block_name)

        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = SCF.serviceConfigFile(self.cfg).getParam('chain', 'outputPath')
        self.field_Region = SCF.serviceConfigFile(self.cfg).getParam('chain', 'regionField')
        self.shape_region = SCF.serviceConfigFile(self.cfg).getParam('chain', 'regionPath')
        self.runs = SCF.serviceConfigFile(self.cfg).getParam('chain', 'runs')
        
    def step_description(self):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Manage indecisions in classification's fusion")
        return description

    def step_inputs(self):
        """
        Return
        ------
            the return could be and iterable or a callable
        """
        from Common import FileUtils as fut
        return fut.FileSearch_AND(os.path.join(self.output_path, "classif"), True, "_FUSION_")

    def step_execute(self):
        """
        Return
        ------
        lambda
            the function to execute as a lambda function. The returned object
            must be a lambda function.
        """
        from Classification import NoData as ND
        step_function = lambda x: ND.noData(self.output_path,
                                            x,
                                            self.field_Region,
                                            os.path.join(self.output_path, "features"),
                                            self.shape_region,
                                            self.runs,
                                            self.cfg,
                                            self.workingDirectory)
        return step_function

    def step_outputs(self):
        """
        """
        pass
